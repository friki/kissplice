/* ***************************************************************************
 *
 *                              KisSplice
 *      de-novo calling alternative splicing events from RNA-seq data.
 *
 * ***************************************************************************
 *
 * Copyright INRIA 
 *  contributors :  Vincent Lacroix
 *                  Pierre Peterlongo
 *                  Gustavo Sacomoto
 *                  Vincent Miele
 *                  Alice Julien-Laferriere
 *                  David Parsons
 *
 *
 *
 * pierre.peterlongo@inria.fr
 * vincent.lacroix@univ-lyon1.fr
 *
 * This software is a computer program whose purpose is to detect alternative
 * splicing events from RNA-seq data.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */





#ifndef NNODE_H
#define NNODE_H


// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include <cstdio>
#include <cstdlib>
#include <list>




// ===========================================================================
//                             Include Project Files
// ===========================================================================
#include "NEdge.h"




// ===========================================================================
//                              Class declarations
// ===========================================================================




// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================






class NNode
{
  public :

    // =======================================================================
    //                                 Enums
    // =======================================================================

    // =======================================================================
    //                               Constructors
    // =======================================================================
    NNode( void );

    // =======================================================================
    //                               Destructors
    // =======================================================================

    // =======================================================================
    //                            Accessors: getters
    // =======================================================================
    inline list<NEdge>& getMAdjList( void );
    inline const list<NEdge>& getAdjList( void ) const;
    inline const string& getSequence( void ) const;

    // =======================================================================
    //                            Accessors: setters
    // =======================================================================
    inline void setSequence( string seq );

    // =======================================================================
    //                                Operators
    // =======================================================================

    // =======================================================================
    //                              Public Methods
    // =======================================================================
    void add_to_adj_list( NEdge new_edge );

    // =======================================================================
    //                             Public Attributes
    // =======================================================================



  protected :

    // =======================================================================
    //                            Forbidden Constructors
    // =======================================================================
    //~ NNode( void )
    //~ {
      //~ printf( "%s:%d: error: call to forbidden constructor.\n", __FILE__, __LINE__ );
      //~ exit( EXIT_FAILURE );
    //~ };
    /*NNode( const NNode& model )
    {
      printf( "%s:%d: error: call to forbidden constructor.\n", __FILE__, __LINE__ );
      exit( EXIT_FAILURE );
    };*/


    // =======================================================================
    //                              Protected Methods
    // =======================================================================

    // =======================================================================
    //                             Protected Attributes
    // =======================================================================
    //! Adjacency list TODO: explain more (in, out, both ?) !
    list<NEdge> _adjList;
    
    //! Node informations, sequences
    string _sequence;
};


// ===========================================================================
//                              Getters' definitions
// ===========================================================================
inline list<NEdge>& NNode::getMAdjList( void )
{
  return _adjList;
}

const list<NEdge>& NNode::getAdjList( void ) const
{
  return _adjList;
}

const string& NNode::getSequence( void ) const
{
  return _sequence;
}

// ===========================================================================
//                              Setters' definitions
// ===========================================================================
void NNode::setSequence( string seq )
{
  _sequence = seq;
}

// ===========================================================================
//                          Inline Operators' definitions
// ===========================================================================

// ===========================================================================
//                          Inline functions' definition
// ===========================================================================


#endif // NNODE_H
