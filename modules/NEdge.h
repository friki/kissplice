/* ***************************************************************************
 *
 *                              KisSplice
 *      de-novo calling alternative splicing events from RNA-seq data.
 *
 * ***************************************************************************
 *
 * Copyright INRIA 
 *  contributors :  Vincent Lacroix
 *                  Pierre Peterlongo
 *                  Gustavo Sacomoto
 *                  Vincent Miele
 *                  Alice Julien-Laferriere
 *                  David Parsons
 *
 * pierre.peterlongo@inria.fr
 * vincent.lacroix@univ-lyon1.fr
 *
 * This software is a computer program whose purpose is to detect alternative
 * splicing events from RNA-seq data.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".

 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.

 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
 
 
 
 
#ifndef NEDGE_H
#define NEDGE_H


// ===========================================================================
//                               Include Libraries
// ===========================================================================
#include <cstdio>
#include <cstdlib>
#include <string>




// ===========================================================================
//                             Include Project Files
// ===========================================================================




// ===========================================================================
//                             Declare Used Namespaces
// ===========================================================================
using namespace std;


/* The node class is used into the adjacency list of the NGraph object.
It keeps the whole information of the link between a node u and its outgoing
neighbours: the index of the node v and the label of the arc linking u to v.
*/


class NEdge
{
  public :
    
    // =======================================================================
    //                                 Enums
    // =======================================================================
    
    // =======================================================================
    //                               Constructors
    // =======================================================================
    NEdge( const NEdge& model );
    NEdge( int node, string labels );
    NEdge( int node, char label1, char label2 );

    // =======================================================================
    //                               Destructors
    // =======================================================================
    ~NEdge( void );

    // =======================================================================
    //                            Accessors: getters
    // =======================================================================
    inline int    get_node( void ) const;
    inline string get_labels( void ) const;
    

    // =======================================================================
    //                            Accessors: setters
    // =======================================================================
    inline void set_node( int node );
    inline void set_labels( string labels );
    

    // =======================================================================
    //                                Operators
    // =======================================================================

    // =======================================================================
    //                              Public Methods
    // =======================================================================
    void add_label( string label );
    void del_all_but_first( void );
    void revert_dir( int i );

    // =======================================================================
    //                             Public Attributes
    // =======================================================================



  protected :

    // =======================================================================
    //                            Forbidden Constructors
    // =======================================================================
    NEdge( void )
    {
      printf( "%s:%d: error: call to forbidden constructor.\n", __FILE__, __LINE__ );
      exit( EXIT_FAILURE );
    };


    // =======================================================================
    //                              Protected Methods
    // =======================================================================

    // =======================================================================
    //                             Protected Attributes
    // =======================================================================
    //! Index of the node 
    int node;
  
    //! Label of the incoming edge
    string labels;
};


// ===========================================================================
//                              Getters' definitions
// ===========================================================================
int NEdge::get_node( void ) const
{
  return node;
};

string NEdge::get_labels( void ) const
{
  return labels;
};


// ===========================================================================
//                              Setters' definitions
// ===========================================================================
void NEdge::set_node( int _node )
{
  node = _node;
};

void NEdge::set_labels( string _labels )
{
  labels = _labels;
};


// ===========================================================================
//                          Inline Operators' definitions
// ===========================================================================

// ===========================================================================
//                          Inline functions' definition
// ===========================================================================


#endif // NEDGE_H
