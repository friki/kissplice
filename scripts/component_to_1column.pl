#!/usr/bin/perl -w


if(@ARGV !=2){
print "format:component_to_1_column.pl file_component file_1column \n";
print "Please specify the names of the input and output files\n"; 
exit 1;
}

$infile=shift;  #idio me $infile=$ARGV[0]
$outfile1=shift; #idio me $outfile= $ARGV[1]

open(IN, "$infile") || die "Cannot open: $!\n";

open(OUT, ">$outfile1") || die "Cannot open: $!\n";

#24	287	RR
#24	0	RR
#24	25	FF
#1099	2009	FR
#1099	1073	FF
#1099	1100	RF
#1103	1990	RF
#1103	1100	FR

$count=0;

while($line=<IN>){
#if(($line=~m/^(\d+)\t(\d+) \((\w)(\w)\) /)){
if($line=~m/^(\d+)(.*)/){ 
   print OUT "Component: $count\n";
chomp($line);

@column=split( /\s/, $line);


    foreach $element(@column){
print OUT "$element\n";
    }

    $count++;
}
}

close(IN);
close (OUT);
