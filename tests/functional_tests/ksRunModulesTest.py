#!/usr/bin/env python
import re
import fnmatch
import os
from sys import argv
from os.path import dirname, abspath
import ProcessLauncher
TEST_INSTDIR=dirname(abspath(argv[0])) 

command_line= argv[1]+"/ks_run_modules "+TEST_INSTDIR+"/data/graph_HBM75brainliver_10000_k25.edges "+TEST_INSTDIR+"/data/graph_HBM75brainliver_10000_k25.nodes 25 "+TEST_INSTDIR+"/data/test_ks_run_modules_output_graph"

result = ProcessLauncher.run(command_line)
print result

# testing expected results
successful = True
if not (re.search('Number of biconnected components found: 9', result)):
    successful = False
    
#removing the output files
for root,dirs,files in os.walk(TEST_INSTDIR+"/data"):
    for filename in fnmatch.filter(files,'test_ks_run_modules_output*'):
        os.remove(os.path.join(root,filename))
# summary
if successful:
    print "ks_run_modules: test SUCCESSFUL"
else:
    print "ks_run_modules: test FAILED"