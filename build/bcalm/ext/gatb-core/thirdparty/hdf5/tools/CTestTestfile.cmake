# CMake generated Testfile for 
# Source directory: /home/vincent/KisSplice/kissplice/bcalm/gatb-core/gatb-core/thirdparty/hdf5/tools
# Build directory: /home/vincent/KisSplice/kissplice/build/bcalm/ext/gatb-core/thirdparty/hdf5/tools
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("lib")
subdirs("h5dump")
