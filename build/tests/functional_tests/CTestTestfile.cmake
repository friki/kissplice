# CMake generated Testfile for 
# Source directory: /home/vincent/KisSplice/kissplice/tests/functional_tests
# Build directory: /home/vincent/KisSplice/kissplice/build/tests/functional_tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(ksBubbleEnumerationTest "/home/vincent/KisSplice/kissplice/tests/functional_tests/ksBubbleEnumerationTest.py" "/home/vincent/KisSplice/kissplice/build/libexec/kissplice")
set_tests_properties(ksBubbleEnumerationTest PROPERTIES  PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
add_test(ksRunModulesTest "/home/vincent/KisSplice/kissplice/tests/functional_tests/ksRunModulesTest.py" "/home/vincent/KisSplice/kissplice/build/libexec/kissplice")
set_tests_properties(ksRunModulesTest PROPERTIES  PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
add_test(ksErrorRemovalTest "/home/vincent/KisSplice/kissplice/tests/functional_tests/ksErrorRemovalTest.py" "/home/vincent/KisSplice/kissplice/build/libexec/kissplice")
set_tests_properties(ksErrorRemovalTest PROPERTIES  PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
