# CMake generated Testfile for 
# Source directory: /home/vincent/KisSplice/kissplice/tests
# Build directory: /home/vincent/KisSplice/kissplice/build/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("integration_tests")
subdirs("functional_tests")
