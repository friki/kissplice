# CMake generated Testfile for 
# Source directory: /home/vincent/KisSplice/kissplice/tests/integration_tests
# Build directory: /home/vincent/KisSplice/kissplice/build/tests/integration_tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(KisspliceBasicTest "/home/vincent/KisSplice/kissplice/build/bin/kissplice" "-h")
add_test(KisspliceDBGTest "/home/vincent/KisSplice/kissplice/tests/integration_tests/kisspliceDBGTest.py" "/home/vincent/KisSplice/kissplice/build/bin")
set_tests_properties(KisspliceDBGTest PROPERTIES  PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
add_test(KisspliceTwoSequencesTest "/home/vincent/KisSplice/kissplice/tests/integration_tests/kisspliceTwoSequencesTest.py" "/home/vincent/KisSplice/kissplice/build/bin")
set_tests_properties(KisspliceTwoSequencesTest PROPERTIES  PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
add_test(KisspliceGraphAndSequenceTest "/home/vincent/KisSplice/kissplice/tests/integration_tests/kisspliceGraphAndSequenceTest.py" "/home/vincent/KisSplice/kissplice/build/bin")
set_tests_properties(KisspliceGraphAndSequenceTest PROPERTIES  PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
add_test(KisspliceGraphTest "/home/vincent/KisSplice/kissplice/tests/integration_tests/kisspliceGraphTest.py" "/home/vincent/KisSplice/kissplice/build/bin")
set_tests_properties(KisspliceGraphTest PROPERTIES  PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
