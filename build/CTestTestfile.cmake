# CMake generated Testfile for 
# Source directory: /home/vincent/KisSplice/kissplice
# Build directory: /home/vincent/KisSplice/kissplice/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("bcalm")
subdirs("thirdparty")
subdirs("modules")
subdirs("man")
subdirs("doc")
subdirs("tests")
